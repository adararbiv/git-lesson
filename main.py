from time import time


def timeit(func):
    def wrap(*args, **kwargs):
        a = time()
        res = func(*args, **kwargs)
        b = time()
        print(f"{a - b = }")
        return res
    return wrap


@timeit
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
